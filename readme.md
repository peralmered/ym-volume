# YM-volume
*Python3+pygame+xialib*

Just a simple tool to visualize what YM2149 volume levels actually output to.

### Known issues

* If the main rectangle in the window clips, background rectangles go nuts

