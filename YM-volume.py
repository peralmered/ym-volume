#!/usr/bin/env python
# -*- coding: utf-8 -*-

strVersion="0.01"
strProgramHeaderShort="YM-volume.py v"+strVersion
strProgramHeader="---[ "+strProgramHeaderShort+" ]------------------------------------------------"
strProgramDescription="[ YM-volume ]"

"""

===============================================================================================
===============================================================================================
== ToDo


# Bug: If the main window box is clipped, the background squares go nuts


== ToDo
===============================================================================================
===============================================================================================
== Thoughts


== Thoughts
===============================================================================================
===============================================================================================

"""

boolAllowScreenOutput=True

if boolAllowScreenOutput: print(strProgramHeader)

boolDebug=False  # True

boolDebugPrinting=False

################################################################################################
################################################################################################
## Globals


###############################################################################
## Editor values

arrYmData=[0, 0, 0, 4, 8, 14, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0]

arrHexVals=["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"]

## Editor values
###############################################################################
## Sizes of stuffs and ...things


numScreenWidth=0
numScreenHeight=0

numWindowWidth=1800
numWindowHeight=960

numTargetScreenWidth=320
numTargetScreenHeight=200

numTargetScaleIndex=2
arrTargetScales=[]
arrTargetScales.append({"str": "16:1", "val": 16.0})
arrTargetScales.append({"str": "8:1", "val": 8.0})
arrTargetScales.append({"str": "4:1", "val": 4.0})
arrTargetScales.append({"str": "2:1", "val": 2.0})
arrTargetScales.append({"str": "1:1", "val": 1.0})
arrTargetScales.append({"str": "1:2", "val": 0.5})
arrTargetScales.append({"str": "1:4", "val": 0.25})
arrTargetScales.append({"str": "1:8", "val": 0.125})
numTargetScale=arrTargetScales[numTargetScaleIndex]["val"]

numTargetBorderSize=2

numInfoFontSize=20
numInfoLineHeight=24
numInfoPositionX=20
numInfoPositionY=20


## Sizes of stuffs and ...things
###############################################################################
## Colors


colBackground=64,64,64
colTargetWindow=0,0,0
colTargetFrame=191,191,191
colText=255,255,255
colLineDuringCreation=255,255,0
colLineRegular=127,127,127
colLineSelected=255,255,255
colHandleLineEnd=127-32,191-32,127-32
colHandleLineEndSelected=191,255,191
colHandleCurve=127-32,127-32,191-32
colHandleCurveSelected=191,191,255
colHandleShadow=0,0,0
colBezierHelpline=127,127,191

colPreviewPlot=255,127,127


## Colors
###############################################################################
## Other ...entities


numEditorFrameRate=120.0
numPlaybackFrameRate=50.0
numFrameRate=numEditorFrameRate
screen=None
myClock=None
fontInfo=None

twtopx=None
twtopy=None

arrElements=[]

numMode=0  # Hardcoded

numStartX=None
numStartY=None

numCurrentElement=None
numCurrentHandle=2

strWaitingForClickMessage=""

numEditedSpeed=0

arrPlots=[]
arrOutPlots=[]

arrNumKeysValues=[]
arrNumKeys=[]

numAnimFrame=0

strLatestPathFilename=""
strLatestExportFilename=""


## Other ...entities
###############################################################################


## Globals
################################################################################################
################################################################################################
## Defines


#----------------------------------------------------------------------------------------------


## Defines
################################################################################################
################################################################################################
## Constants


## Constants
################################################################################################
################################################################################################
## Functions


def GetLineBrokenString(strIn, numMaxWidth):
  arrOut=[]
  numWidth, numHeight=fontInfo.size(strIn)
  if numWidth>numMaxWidth:
    # Find a space position that leaves us with a short enough string
    arrSpacePositions=[]
    for i, c in enumerate(strIn):
      if c==" ":
        arrSpacePositions.append(i)
    if arrSpacePositions:
      arrSpacePositions=list(reversed(arrSpacePositions))
      boolHit=False
      for ii, p in enumerate(arrSpacePositions):
        strLeftText=strIn[:p]
        #strRightText=strText[p+1:]
        numW, numH=fontInfo.size(strLeftText)
        if numW<numMaxWidth:
          boolHit=True
          numSpacePos=p
          break
      if boolHit:
        strLeftText=strIn[:numSpacePos]
        strRightText=strIn[numSpacePos+1:]
        arrOut.append(strLeftText)
        arrOut.append(strRightText)
        return arrOut
    else:
      # No spaces found, just accept the string as-is
      arrOut.append(strIn)
      return arrOut
  arrOut.append(strIn)
  return arrOut


#----------------------------------------------------------------------------------------------


def DrawText(strText, x, y, colText):
  numMaxTextWidth=numWindowWidth-(numInfoPositionX*2)

  #############################################################
  ## Insert line breaks if necessary  

  arrLineBrokenText=[]
  arrText=strText.split("\n")
  for t in arrText:
    boolLoop=True
    while boolLoop:
      arrResult=GetLineBrokenString(t, numMaxTextWidth)
      arrLineBrokenText.append(arrResult[0])
      if len(arrResult)==1:
        boolLoop=False
      else:
        t=arrResult[1]

  ## Insert line breaks if necessary
  #############################################################

  #for thisText in arrText:
  for thisText in arrLineBrokenText:
    label = fontInfo.render(thisText, 1, colText)
    screen.blit(label, (x, y))
    y+=numInfoLineHeight


#----------------------------------------------------------------------------------------------


def PutTargetPixel(tx, ty, col):
  global screen
  global twtopx
  global twtopy
  size=numTargetScale
  if size<1:
    size=1
  pxRect=(twtopx+(tx*numTargetScale), twtopy+(ty*numTargetScale), size, size)
  screen.fill(col, pxRect)


#----------------------------------------------------------------------------------------------


def PutTargetHorLine(tx0, tx1, ty, col):
  if tx1<tx0:
    numTemp=tx0
    tx0=tx1
    tx1=numTemp

  global screen
  global twtopx
  global twtopy
  size=numTargetScale
  if size<1:
    size=1
  x0=twtopx+(tx0*numTargetScale)
  x1=twtopx+(tx1*numTargetScale)
  width=x1-x0
  pxRect=(x0, twtopy+(ty*numTargetScale), width, size)
  screen.fill(col, pxRect)    


#----------------------------------------------------------------------------------------------


def PutTargetRect(tx0, tx1, ty0, ty1, col):
  if tx1<tx0:
    numTemp=tx0
    tx0=tx1
    tx1=numTemp

  if ty1<ty0:
    numTemp=ty0
    ty0=ty1
    ty1=numTemp

  global screen
  global twtopx
  global twtopy
  size=numTargetScale
  if size<1:
    size=1
  x0=twtopx+(tx0*numTargetScale)
  x1=twtopx+(tx1*numTargetScale)
  width=x1-x0
  y0=twtopy+(ty0*numTargetScale)
  y1=twtopy+(ty1*numTargetScale)
  height=y1-y0
  pxRect=(x0, y0, width, height)
  screen.fill(col, pxRect)    


#----------------------------------------------------------------------------------------------


def DrawBresenhamSimple(x0, y0, x1, y1, col):
#def DrawBresenham_Wiki_Optimization(x0,y0,  x1,y1):
  # Wikipedia's "Optimization" routine
  # http://en.wikipedia.org/wiki/Bresenham's_line_algorithm#Optimization
  x0=int(round(x0))
  y0=int(round(y0))
  x1=int(round(x1))
  y1=int(round(y1))

  boolYMajor=False
  if abs(y1-y0)>abs(x1-x0):
    boolYMajor=True
    # swap x0 and y0
    temp=x0
    x0=y0
    y0=temp
    # swap x1 and y1
    temp=x1
    x1=y1
    y1=temp
    
  if x0>x1:
    # swap x0 and x1
    temp=x0
    x0=x1
    x1=temp
    # swap y0 and y1
    temp=y0
    y0=y1
    y1=temp

  dx=x1-x0
  dy=abs(y1-y0)
  error=dx/2
  y=y0
  ystep=-1
  if y0<y1:
    ystep=1

  x=x0
  for n in range(dx+1):
    if boolYMajor:
      PutTargetPixel(y, x, col)
    else:
      PutTargetPixel(x, y, col)
    error-=dy
    if error<0:
      y+=ystep
      error+=dx
    x+=1


#----------------------------------------------------------------------------------------------


def Init():
  dummy=True


#----------------------------------------------------------------------------------------------




## Functions
################################################################################################
################################################################################################


if boolDebug:
  print("Importing libraries...",)
import sys
import os
import subprocess
#from PIL import Image
#from PIL import ImageColor
import argparse
import time
import copy
import random
import math

sys.path.insert(0, '/projekt/_tools/xia_lib')
import xia
from xia import Fatal
from xia import Warn
from xia import Notice
from xia import Out
#
#import pdb

import json

from tkinter import *
import tkinter, tkinter.constants, tkinter.filedialog
 
import pygame
from pygame.locals import *


if boolDebug:
  print("done\n")


################################################################################################
################################################################################################
## Test code


if 1==2:
  
  print(str(pygame.display.info()))
  exit(0)

  
  arrA=[]
  arrA.append({"letter": "a", "number": 4})
  arrA.append({"letter": "b", "number": 9})
  arrA.append({"letter": "c", "number": 3})
  arrA.append({"letter": "d", "number": 7})
  arrA.append({"letter": "e", "number": 6})
  arrA.append({"letter": "f", "number": 0})

  for i,a in enumerate(arrA):
    print(str(i)+": "+str(a))

  print("\n"+"-"*88+"\n")

  InsertMe={"letter": "X", "number": 3.14}
  arrA.insert(3, InsertMe)

  for i,a in enumerate(arrA):
    print(str(i)+": "+str(a))
  
  exit(0)


## Test code
################################################################################################
################################################################################################
## Handle input parameters


objParser = argparse.ArgumentParser(description=strProgramDescription)

objParser.add_argument("-i", "--infile",
                       dest="infile",
                       help=".path file stub to load",
                       metavar="FILE",
                       required=False)
objParser.add_argument("-o", "--outfile",
                       dest="outfile",
                       help=".s file to export to",
                       metavar="FILE",
                       required=False)


# Parameter defaults
boolInFileGiven=False
strInFile=""
boolOutFileGiven=False
strOutFile=""
boolVerboseMode=False

numArgs=0
Args=vars(objParser.parse_args())

for key in Args:
  numArgs+=1
  strValue=str(Args[key])
  #print(str(key)+" = "+str(strValue))
  if key=="infile" and strValue!="None":
    boolInFileGiven=True
    strInFile=strValue
  elif key=="outfile" and strValue!="None":
    boolOutFileGiven=True
    strOutFile=strValue
  elif key=="verbose" and strValue!="None":
    boolVerboseMode=xia.StrToBool(strValue)

# No args = print usage and exit
if numArgs==0:
  objParser.print_help()
  exit(0)


# Test infile
if boolInFileGiven:
  numResult=xia.CheckFileExistsAndReadable(strInFile)
  if numResult!=0:
    if numResult&xia.FILE_EXISTS_FAIL:
      Fatal("Can't find file \""+strInFile+"\"!")
    if numResult&xia.FILE_READABLE_FAIL:
      Fatal("Can't read file \""+strInFile+"\"!")


# Export without opening window
if boolInFileGiven:
  strFilename=strInFile

  boolFileLoadedOk=True
  # Test infile
  numResult=xia.CheckFileExistsAndReadable(strFilename)
  if numResult!=0:
    if numResult&xia.FILE_EXISTS_FAIL:
      Fatal("Error: Can't find file \""+strFilename+"\"!")
      boolFileLoadedOk=False
    if numResult&xia.FILE_READABLE_FAIL:
      Fatal("Error: Can't read file \""+strFilename+"\"!")
      boolFileLoadedOk=False
  if boolFileLoadedOk:
    LoadFile(strFilename)
    print("Opened file \""+strFilename+"\".")

    RenderLinesAndBeziers(False)
    BuildPlotPreviewPath()
    if not arrOutPlots:
      Fatal("No data to export!")
    else:
      boolOutOfRange=False
      arrResult=BuildExportData(arrOutPlots)
      if arrResult["success"]==False:
        boolOutOfRange=False
      else:
        arrExportData=arrResult["data"]

      if boolOutOfRange:
        Fatal("Some data is outside word range (-32768 -- 32767) - export cancelled!")
      else:
        if boolOutFileGiven:
          strFilename=strOutFile
        else:
          if xia.EndsWith(strFilename, ".path"):
            strFilename=strFilename[:-len(".path")]
          if not xia.EndsWith(strFilename, ".s"):
            strFilename+=".s"
          Notice("No output filename given, output will be saved as \""+strFilename+"\".")
        xia.WriteTextFile(arrExportData, strFilename)
        print("Data exported to \""+strFilename+"\".")

  exit(0)


## Handle input parameters
################################################################################################
################################################################################################
## Tkinter


tkRoot=None


#----------------------------------------------------------------------------------------------


def InitTk():
  global tkRoot
  tkRoot=Tk()  # Create root window
  tkRoot.withdraw()  # Hide the god ugly root window


#----------------------------------------------------------------------------------------------


def GetExportFileName(strFile=""):
  arrFileTypes=[]
  arrFileTypes.append(('.s files', '.s'))
  arrFileTypes.append(('all files', '.*'))
  #arrFileTypes.append(('text files', '.txt'))
  strFilename=tkFileDialog.asksaveasfilename(parent=tkRoot,
                                             initialdir=os.getcwd(),
                                             initialfile=strFile,
                                             title="Please select a filename for exporting:",
                                             filetypes=arrFileTypes)
  return strFilename


## Tkinter
################################################################################################
################################################################################################
## Main loop


def main():
  global numScreenWidth
  global numScreenHeight
  global numWindowWidth
  global numWindowHeight
  global screen
  global myClock
  global fontInfo
  global twtopx
  global twtopy
  global arrElements
  global numMode
  global numStartX
  global numStartY
  global numCurrentElement
  global dummyElement
  global numCurrentHandle
  global strWaitingForClickMessage
  global numTargetScale
  global numTargetScaleIndex
  global numSquareHandleSize
  global numCircleHandleRadius
  global numHandleLineWidth
  global numEditedSpeed
  global numFrameRate
  global arrPlots
  global arrNumKeysValues
  global arrNumKeys
  global numTargetScreenWidth
  global numTargetScreenHeight
  global numAnimFrame
  global numPlaybackFrameRate
  global strLatestPathFilename
  global strLatestExportFilename


  Init()

  InitTk()

  numScreenWidth=tkRoot.winfo_screenwidth()
  numScreenHeight=tkRoot.winfo_screenheight()
  numWindowWidth=int(numScreenWidth*0.9)
  numWindowHeight=int(numScreenHeight-110)

  #os.environ['SDL_VIDEO_CENTERED']="1"
  os.environ['SDL_VIDEO_WINDOW_POS'] = '%i,%i' % (40, 40)

#  stdout=sys.__stdout__
#  stderr=sys.__stderr__
#  sys.stdout=open(os.devnull,'w')
#  sys.stderr=open(os.devnull,'w')
  pygame.init()
#  sys.stdout=stdout
#  sys.stderr=stderr

#  # Load and set logo
#  logo = pygame.image.load(strDataFolder+os.sep+"logo.png")
#  pygame.display.set_icon(logo)
  pygame.display.set_caption(strProgramHeaderShort)
  screen = pygame.display.set_mode((numWindowWidth,numWindowHeight), HWSURFACE|DOUBLEBUF|RESIZABLE)
  fontInfo=pygame.font.SysFont("Calibri", numInfoFontSize, bold=True)
  pygame.mouse.set_visible(True)
  myClock=pygame.time.Clock()


  boolRunning = True

  # Main loop
  while boolRunning:
    
    # Some coords math
    cx=numWindowWidth/2
    cy=numWindowHeight/2    
    tw=(numTargetScreenWidth*numTargetScale)
    th=(numTargetScreenHeight*numTargetScale)
    bs=numTargetBorderSize
    twtopx=cx-(tw/2)  # target window top X
    twtopy=cy-(th/2)  # target window top Y
    twtopy+=(numInfoFontSize+numInfoPositionY)/2

    # Get mouse pos and convert to target coords
    m=pygame.mouse.get_pos()
    numMouseX=m[0]
    numMouseY=m[1]
    numTargetMouseX=numMouseX-twtopx
    numTargetMouseY=numMouseY-twtopy
    numTargetMouseX/=numTargetScale
    numTargetMouseY/=numTargetScale
    numTargetMouseX=int(numTargetMouseX)
    numTargetMouseY=int(numTargetMouseY)

    numKeyboardModifiers=pygame.key.get_mods()
    for event in pygame.event.get():

      if event.type==VIDEORESIZE:
        arrNewSize=event.dict['size']
        numWindowWidth=arrNewSize[0]
        numWindowHeight=arrNewSize[1]
        screen=pygame.display.set_mode( (numWindowWidth, numWindowHeight) , HWSURFACE|DOUBLEBUF|RESIZABLE)

        #######################################################################
        ## Recalculate stuff related to target window and the scaling thereof

        cx=numWindowWidth/2
        cy=numWindowHeight/2

        # If Targetscreen is too large for new window size, scale it down
        if tw>numWindowWidth or th>numWindowHeight:
          while (tw>numWindowWidth or th>numWindowHeight) and numTargetScaleIndex<len(arrTargetScales)-1:
            if numTargetScaleIndex<len(arrTargetScales)-1:
              numTargetScaleIndex+=1
              numTargetScale=arrTargetScales[numTargetScaleIndex]["val"]
            tw=(numTargetScreenWidth*numTargetScale)
            th=(numTargetScreenHeight*numTargetScale)
        tw=(numTargetScreenWidth*numTargetScale)
        th=(numTargetScreenHeight*numTargetScale)
        bs=numTargetBorderSize
        twtopx=cx-(tw/2)  # target window top X
        twtopy=cy-(th/2)  # target window top Y
        twtopy+=(numInfoFontSize+numInfoPositionY)/2


        numTargetMouseX=numMouseX-twtopx
        numTargetMouseY=numMouseY-twtopy
        numTargetMouseX/=numTargetScale
        numTargetMouseY/=numTargetScale
        numTargetMouseX=int(numTargetMouseX)
        numTargetMouseY=int(numTargetMouseY)

        ## Recalculate stuff related to target window and the scaling thereof
        #######################################################################


      if event.type==pygame.QUIT:
        exit(0)

      if event.type==KEYDOWN:
        if event.key==K_ESCAPE:
          exit(0)


      boolMouseClick=False
      if event.type==MOUSEBUTTONDOWN:
        if event.button==4:
          if numTargetScaleIndex>0:
            numTargetScaleIndex-=1
            numTargetScale=arrTargetScales[numTargetScaleIndex]["val"]
        elif event.button==5:
          if numTargetScaleIndex<len(arrTargetScales)-1:
            numTargetScaleIndex+=1
            numTargetScale=arrTargetScales[numTargetScaleIndex]["val"]
        else:
          boolMouseClick=True



    ###########################################################################
    ###########################################################################
    ## Render

    mx=(numMouseX-twtopx)/numTargetScale
    my=(numMouseY-twtopy)/numTargetScale
    numScreenMouseX=math.floor(mx)
    numScreenMouseY=math.floor(my)

    ###########################################################################
    ## Draw background


    screen.fill(colBackground)


    ## Draw background
    ###########################################################################


    boolRenderEditorStuff=True



    ###########################################################################
    ## Draw target window


    if boolRenderEditorStuff or boolRenderPlotPreview or boolRenderAnimatedPreview:
      # Border of target rectangle
      screen.fill(colTargetFrame, (twtopx-bs, twtopy-bs, tw+(bs*2), th+(bs*2)))
      # Inside of target rectangle
      screen.fill(colTargetWindow, (twtopx, twtopy, tw, th))


    ## Draw target window
    ###########################################################################
    ## Draw target window contents


    # Mouse squares
    colSquare=255, 157, 31
    colSquareHover=255, 255, 255
    colSquareSelected=255, 216, 171
    colBackLight=137, 137, 137
    colBackDark=111, 111, 111

    numSize=7
    numSpace=2 # Must be even
    xstart=8
    ystart=8

    ###########################################################
    ## Backgrounds

    ###########################################
    ## Dark one...

    numBlockSize=numSize+numSpace
    xend=xstart+16*numBlockSize
    yend=ystart+16*numBlockSize
    PutTargetRect(xstart, xend, ystart, yend, colBackDark)

    ## Dark one...
    ###########################################
    ## Light ones....

    y0=ystart
    x0=xstart+4*numBlockSize
    x1=x0+4*numBlockSize
    y1=y0+4*numBlockSize
    PutTargetRect(x0, x1, y0, y1, colBackLight)
    x0=xstart+12*numBlockSize
    x1=x0+4*numBlockSize
    PutTargetRect(x0, x1, y0, y1, colBackLight)

    y0+=8*numBlockSize
    x0=xstart+4*numBlockSize
    x1=x0+4*numBlockSize
    y1=y0+4*numBlockSize
    PutTargetRect(x0, x1, y0, y1, colBackLight)
    x0=xstart+12*numBlockSize
    x1=x0+4*numBlockSize
    PutTargetRect(x0, x1, y0, y1, colBackLight)

    y0=ystart+4*numBlockSize
    x0=xstart
    x1=x0+4*numBlockSize
    y1=y0+4*numBlockSize
    PutTargetRect(x0, x1, y0, y1, colBackLight)
    x0+=8*numBlockSize
    x1=x0+4*numBlockSize
    PutTargetRect(x0, x1, y0, y1, colBackLight)

    y0+=8*numBlockSize
    x0=xstart
    x1=x0+4*numBlockSize
    y1=y0+4*numBlockSize
    PutTargetRect(x0, x1, y0, y1, colBackLight)
    x0+=8*numBlockSize
    x1=x0+4*numBlockSize
    PutTargetRect(x0, x1, y0, y1, colBackLight)

    ## Light ones....
    ###########################################

    ## Backgrounds
    ###########################################################
    ## Squares

    arrSquares=[]

    for x in range(16):
      for y in range(16):
        thisX=(numSpace/2)+xstart+x*(numSize+numSpace)
        thisY=(numSpace/2)+ystart+y*(numSize+numSpace)
        thisXend=thisX+numSize
        thisYend=thisY+numSize
        thisCol=colSquare
        boolSelected=False
        if arrYmData[x]==(15-y):
          boolSelected=True
        if boolSelected:
          thisCol=colSquareSelected
        PutTargetRect(thisX, thisXend, thisY, thisYend, thisCol)
        thisSquare={}
        thisSquare["x0"]=thisX
        thisSquare["x1"]=thisXend
        thisSquare["y0"]=thisY
        thisSquare["y1"]=thisYend

        cx=thisX+(numSize/2)
        cy=thisY+(numSize/2)
        thisSquare["cx"]=twtopx+(cx*numTargetScale)
        thisSquare["cy"]=twtopy+(cy*numTargetScale)

        thisSquare["ym"]=15-y
        thisSquare["step"]=x

        arrSquares.append(thisSquare)

    ## Squares
    ###########################################################
    ## Selected square

    for thisSquare in arrSquares:
      if numScreenMouseX>=thisSquare["x0"]-(numSpace/2) and numScreenMouseX<thisSquare["x1"]+(numSpace/2):
        if numScreenMouseY>=thisSquare["y0"]-(numSpace/2) and numScreenMouseY<thisSquare["y1"]+(numSpace/2):
          #thisSquare["selected"]=True

          #if boolMouseClick:
          boolMouseButtonDown=False
          if pygame.mouse.get_pressed()[0]:
            try:
              boolMouseButtonDown=True
            except AttributeError:
              pass
          if boolMouseButtonDown:
            arrYmData[thisSquare["step"]]=thisSquare["ym"]
          PutTargetRect(thisSquare["x0"]-(numSpace/2), thisSquare["x1"]+(numSpace/2), thisSquare["y0"]-(numSpace/2), thisSquare["y1"]+(numSpace/2), colSquareHover)

    ## Selected square
    ###########################################################

    #DrawBresenhamSimple(x0, y0, x1, y1, col)

    ###########################################################
    ## YM values

    arrYmLevels=[0.0, 0.005, 0.008, 0.012, 0.018, 0.024, 0.036, 0.048, 0.069, 0.095, 0.139, 0.191, 0.287, 0.407, 0.648, 1.0]

    xstart=160+8
    numBlockSize=numSize+numSpace
    xend=xstart+16*numBlockSize
    yend=ystart+16*numBlockSize
    PutTargetRect(xstart, xend, ystart, yend, colBackDark)
    
    dotx=xstart+(numSize/2)+(numSpace/2)
    dotx=arrSquares[0]["cx"]+(160+(numSpace/2))*numTargetScale
    doty=arrSquares[-1]["cy"]
    dotymin=doty
    dotymax=arrSquares[0]["cy"]

    
    arrYvalues=[]
    for thisYM in arrYmLevels:
      thisY=dotymin-(thisYM*(abs(dotymax-dotymin)))
      arrYvalues.append(thisY)

    numDotSize=int(2.5*numTargetScale)
    numLineSize=int(1.0*numTargetScale)
    numPrevX=dotx
    numPrevY=arrYvalues[arrYmData[0]]
    x=11
    x=twtopx+(x*numTargetScale)
    y=153
    y=twtopy+(y*numTargetScale)
    for i, thisYM in enumerate(arrYmData):
      doty=arrYvalues[thisYM]
      pygame.draw.circle(screen, colSquare, (int(dotx), int(doty)), numDotSize, 0)
      if i>0:
        pygame.draw.line(screen, colSquare, (numPrevX, numPrevY), (dotx, doty), numLineSize)
      numPrevX=dotx
      numPrevY=doty
      dotx+=(numSize+numSpace)*numTargetScale

      # Hex values
      strText=arrHexVals[thisYM]
      
      print(x, y)
      DrawText(strText, int(x), int(y), colSquare)
      x+=(numSize+numSpace)*numTargetScale
      

    #PutTargetPixel(dotx, dotymin, colSquareHover)
    #PutTargetPixel(dotx, dotymax, colSquareHover)
    #PutTargetRect(dotx-(numSize/2), dotx+(numSize/2), doty-(numSize/2), doty+(numSize/2), colSquareHover)

    ## YM values
    ###########################################################
    

    ## Draw target window contents
    ###########################################################################
    ## Draw info texts


    strText="Zoom level: "+str(arrTargetScales[numTargetScaleIndex]["str"])
    #strText+=" Mouse: ("+str(numScreenMouseX)+", "+str(numScreenMouseY)+")"
    DrawText(strText, numInfoPositionX, numInfoPositionY, colText)


    ## Draw info texts
    ###########################################################################
    ## Finish up and flip


    myClock.tick(numFrameRate)
    pygame.display.flip()


    ## Finish up and flip
    ###########################################################################


    ## Render
    ###########################################################################
    ###########################################################################


## Main loop
################################################################################################
################################################################################################

# run the main function only if this module is executed as the main script
# (if you import this as a module then nothing is executed)
if __name__=="__main__":
  # call the main function
  main()

